-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:8888
-- Время создания: Окт 07 2017 г., 04:30
-- Версия сервера: 5.5.48
-- Версия PHP: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `demo_lyceum`
--

-- --------------------------------------------------------

--
-- Структура таблицы `communities`
--

CREATE TABLE IF NOT EXISTS `communities` (
  `community_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `communities`
--

INSERT INTO `communities` (`community_id`, `name`, `description`) VALUES
(1, 'Без спільноти', ''),
(2, 'Ліцеїсти', 'Спільнота ліцеїстів та випускників ліцею'),
(3, 'Вчителі', 'Спільнота викладачів ліцею');

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `group_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `community_id` int(11) NOT NULL,
  `graduation_year` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`group_id`, `name`, `community_id`, `graduation_year`) VALUES
(1, 'Без групи', 1, 0),
(2, '8-01', 2, 2021),
(3, '8-02', 2, 2021),
(4, '8-03', 2, 2021),
(5, '9-11', 2, 2020),
(6, '9-12', 2, 2020),
(7, '9-13', 2, 2020),
(8, '10-21', 2, 2019),
(9, '10-22', 2, 2019),
(10, '10-23', 2, 2019),
(11, '11-31', 2, 2018),
(12, '11-32', 2, 2018),
(13, '11-33', 2, 2018),
(14, 'Математика', 3, 0),
(15, 'Природничі науки', 3, 0),
(16, 'Гуманітарні науки', 3, 0),
(17, 'Інформатика', 3, 0),
(18, 'Соціально-психологічна служба', 3, 0),
(19, 'Бібліотека', 3, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `positions`
--

CREATE TABLE IF NOT EXISTS `positions` (
  `position_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `positions`
--

INSERT INTO `positions` (`position_id`, `title`) VALUES
(1, 'Ліцеїст'),
(2, 'Директор'),
(3, 'Заступник директора'),
(4, 'Секретар'),
(5, 'Вчитель української мови і літератури'),
(6, 'Вчитель математики'),
(7, 'Вчитель фізики'),
(8, 'Вчитель історії'),
(9, 'Вчитель правознавства'),
(10, 'Вчитель фізкультури'),
(11, 'Вчитель англійської мови'),
(12, 'Вчитель німецької мови'),
(13, 'Вчитель зарубіжної літератури'),
(14, 'Вчитель інформатики'),
(15, 'Вчитель біології'),
(16, 'Вчитель хімії'),
(17, 'Вчитель географії'),
(18, 'Вчитель захисту Вітчизни'),
(19, 'Вчитель основ здоров''я'),
(20, 'Соціальний педагог'),
(21, 'Педагог-організатор'),
(22, 'Психолог'),
(23, 'Бібліотекар');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `role_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `patronymic` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status_text` varchar(255) NOT NULL,
  `position_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=403 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `login`, `password`, `email`, `gender`, `role_id`, `first_name`, `patronymic`, `last_name`, `image`, `status_text`, `position_id`, `group_id`) VALUES
(1, 'YuriyKoshevich', 'ff7ca', 'YuriyKoshevich@gmail.com', 1, 2, 'Юрій', 'Олексійович', 'Кошевич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 2, 16),
(2, 'IrinaBlazhiyevska', 'ad344', 'IrinaBlazhiyevska@gmail.com', 2, 2, 'Ірина', 'Анатоліївна', 'Блажиєвська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 3, 14),
(3, 'IrinaGlibko', '32127', 'IrinaGlibko@gmail.com', 2, 2, 'Ірина', 'Анатоліївна', 'Глібко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 3, 16),
(4, 'AlisaIzyumova', '36206', 'AlisaIzyumova@gmail.com', 2, 2, 'Аліса', 'Валентинівна', 'Ізюмова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 3, 14),
(5, 'MariyaMedvedyeva', '17ffa', 'MariyaMedvedyeva@gmail.com', 2, 2, 'Марія', 'Василівна', 'Медведєва', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 3, 15),
(6, 'TarasSavinkov', '436a9', 'TarasSavinkov@gmail.com', 1, 2, 'Тарас', 'Євгенійович', 'Савінков', '20446f384ca7c6f1d0f59fdc4fdcedd7.png', 'Hello, world!', 3, 16),
(7, 'SvitlanaBabenko', 'c6c23', 'SvitlanaBabenko@gmail.com', 2, 2, 'Світлана', 'Петрівна', 'Бабенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 5, 16),
(8, 'OleksandrBondar', '24108', 'OleksandrBondar@gmail.com', 1, 2, 'Олександр', 'Анатолійович', 'Бондар', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 14, 17),
(9, 'InnaVaskovska', '87407', 'InnaVaskovska@gmail.com', 2, 2, 'Інна', 'Вікторівна', 'Васьковська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 4, 15),
(10, 'LyudmilaVovchuk', 'eff44', 'LyudmilaVovchuk@gmail.com', 2, 2, 'Людмила', 'Миколаївна', 'Вовчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 5, 16),
(11, 'SvitlanaVoynalovich', '8011a', 'SvitlanaVoynalovich@gmail.com', 2, 2, 'Світлана', 'Володимирівна', 'Войналович', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 17, 16),
(12, 'NataliyaGerasimchuk', 'ec136', 'NataliyaGerasimchuk@gmail.com', 2, 2, 'Наталія', 'Володимирівна', 'Герасимчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 5, 16),
(13, 'LinaGodlevska', 'a6385', 'LinaGodlevska@gmail.com', 2, 2, 'Ліна', 'Іванівна', 'Годлевська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 23, 19),
(14, 'OlgaGornostay', '7887c', 'OlgaGornostay@gmail.com', 2, 2, 'Ольга', 'Миколаївна', 'Горностай', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 20, 18),
(15, 'TarasYemec', '7c5aa', 'TarasYemec@gmail.com', 1, 2, 'Тарас', 'Володимирович', 'Ємець', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 7, 15),
(16, 'SergiyZavalnyuk', '84bb4', 'SergiyZavalnyuk@gmail.com', 1, 2, 'Сергій', 'Валентинович', 'Завальнюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 16, 15),
(17, 'YuliyaZelinska', '0cf06', 'YuliyaZelinska@gmail.com', 2, 2, 'Юлія', 'Миколаївна', 'Зелінська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 14, 17),
(18, 'YuliyaKonarivska', '8c283', 'YuliyaKonarivska@gmail.com', 2, 2, 'Юлія', 'Олександрівна', 'Конарівська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 21, 18),
(19, 'GalinaKorniychuk', 'aba8b', 'GalinaKorniychuk@gmail.com', 2, 2, 'Галина', 'Миколаївна', 'Корнійчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 5, 16),
(20, 'OleksandrKuzmenko', '8ry4v', 'OleksandrKuzmenko@gmail.com', 1, 2, 'Олександр', 'Вікторович', 'Кузьменко', '00c4888a384747ee4e9b54b9957be623.jpg', 'Hello, World! My name is Alex Kuzmenko!', 14, 17),
(21, 'OleksandraKuksenko', '38ed5', 'OleksandraKuksenko@gmail.com', 2, 2, 'Олександра', 'Джуманіязовна', 'Куксенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 19, 15),
(22, 'NadiyaKulchicka', 'e89b9', 'NadiyaKulchicka@gmail.com', 2, 2, 'Надія', 'Сергіївна', 'Кульчицька', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 17, 15),
(23, 'IsayLadenzon', '8ac39', 'IsayLadenzon@gmail.com', 1, 2, 'Ісай', 'Теодорович', 'Ладензон', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 6, 14),
(24, 'GalinaLotyuk', 'f9919', 'GalinaLotyuk@gmail.com', 2, 2, 'Галина', 'Миколаївна', 'Лотюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 8, 16),
(25, 'IrinaMiller', 'cd666', 'IrinaMiller@gmail.com', 2, 2, 'Ірина', 'Оскарівна', 'Міллер', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 13, 16),
(26, 'OleksandrMorozov', '66b06', 'OleksandrMorozov@gmail.com', 1, 2, 'Олександр', 'Валерійович', 'Морозов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 6, 14),
(27, 'MarinaMorozova', 'f3faf', 'MarinaMorozova@gmail.com', 2, 2, 'Марина', 'Анатоліївна', 'Морозова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 6, 14),
(28, 'ZoyaMoskalchuk', 'e5cf5', 'ZoyaMoskalchuk@gmail.com', 2, 2, 'Зоя', 'Петрівна', 'Москальчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 6, 14),
(29, 'VolodimirPanskiy', '6e2fe', 'VolodimirPanskiy@gmail.com', 1, 2, 'Володимир', 'Анатолійович', 'Панський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 6, 14),
(30, 'IrinaRakova', 'bb9e8', 'IrinaRakova@gmail.com', 2, 2, 'Ірина', 'Євгеніївна', 'Ракова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 11, 16),
(31, 'OleksandrRassadkin', 'a4e73', 'OleksandrRassadkin@gmail.com', 1, 2, 'Олександр', 'Олегович', 'Рассадкін', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 8, 16),
(32, 'OlenaRizhkina', 'a569e', 'OlenaRizhkina@gmail.com', 2, 2, 'Олена', 'Володимирівна', 'Рижкіна', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 10, 15),
(33, 'AnatoliyRuseckiy', '33efe', 'AnatoliyRuseckiy@gmail.com', 1, 2, 'Анатолій', 'Васильович', 'Русецький', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 18, 15),
(34, 'NataliyaSidorova', '41ffb', 'NataliyaSidorova@gmail.com', 2, 2, 'Наталія', 'Євгенівна', 'Сидорова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 11, 16),
(35, 'LiliyaSokolyuk', '3e295', 'LiliyaSokolyuk@gmail.com', 2, 2, 'Лілія', 'Йосипівна', 'Соколюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 22, 18),
(36, 'OlgaStelmah', 'd092f', 'OlgaStelmah@gmail.com', 2, 2, 'Ольга', 'Ігорівна', 'Стельмах', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 12, 16),
(37, 'MarinaSushicka', 'd88d7', 'MarinaSushicka@gmail.com', 2, 2, 'Марина', 'Анатоліївна', 'Сушицька', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 11, 16),
(38, 'GennadiyFurman', 'd764c', 'GennadiyFurman@gmail.com', 1, 2, 'Геннадій', 'Борисович', 'Фурман', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 11, 16),
(39, 'TetyanaHohlova', 'f505b', 'TetyanaHohlova@gmail.com', 2, 2, 'Тетяна', 'Євгенівна', 'Хохлова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 15, 15),
(40, 'LyudmilaShara', '4effc', 'LyudmilaShara@gmail.com', 2, 2, 'Людмила', 'Миколаївна', 'Шара', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 11, 16),
(41, 'LeonidaShnurkovanyuk', '8b6ca', 'LeonidaShnurkovanyuk@gmail.com', 2, 2, 'Леоніда', 'Олександрівна', 'Шнуркованюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 7, 15),
(42, 'YaroslavBabinov', '6c0ec', 'YaroslavBabinov@gmail.com', 1, 2, 'Ярослав', '', 'Бабінов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(43, 'SofiyaBayun', '9dcb3', 'SofiyaBayun@gmail.com', 2, 2, 'Софія', '', 'Баюн', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(44, 'TetyanaBovsunivska', '78d6a', 'TetyanaBovsunivska@gmail.com', 2, 2, 'Тетяна', '', 'Бовсунівська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(45, 'ArtemGolovachov', '16290', 'ArtemGolovachov@gmail.com', 1, 2, 'Артем', '', 'Головачов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(46, 'OleksandrGordiyenko', '11e45', 'OleksandrGordiyenko@gmail.com', 1, 2, 'Олександр', '', 'Гордієнко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(47, 'OleksiyGorobec', '4fba0', 'OleksiyGorobec@gmail.com', 1, 2, 'Олексій', '', 'Горобець', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(48, 'AnastasiyaGrinchuk-Romanova', '49035', 'AnastasiyaGrinchuk-Romanova@gmail.com', 2, 2, 'Анастасія', '', 'Гринчук-Романова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(49, 'OleksandrDemchenko', 'babe5', 'OleksandrDemchenko@gmail.com', 1, 2, 'Олександр', '', 'Демченко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(50, 'AndriyDemchuk', '22e3b', 'AndriyDemchuk@gmail.com', 1, 2, 'Андрій', '', 'Демчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(51, 'AndriyYefimenko', '76be7', 'AndriyYefimenko@gmail.com', 1, 2, 'Андрій', '', 'Єфіменко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(52, 'AndriyZhuravlov', 'dc64f', 'AndriyZhuravlov@gmail.com', 1, 2, 'Андрій', '', 'Журавльов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(53, 'OlgaKilnicka', '37883', 'OlgaKilnicka@gmail.com', 2, 2, 'Ольга', '', 'Кільніцька', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(54, 'OleksandrKostenko', 'c0e9a', 'OleksandrKostenko@gmail.com', 1, 2, 'Олександр', '', 'Костенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(55, 'OleksiyLazyuta', 'e6e8c', 'OleksiyLazyuta@gmail.com', 1, 2, 'Олексій', '', 'Лазюта', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(56, 'OlgaLisyuk', 'ddcee', 'OlgaLisyuk@gmail.com', 2, 2, 'Ольга', '', 'Лисюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(57, 'Dar''yaLitvinchuk', '52701', 'Dar''yaLitvinchuk@gmail.com', 2, 2, 'Дар''я', '', 'Литвинчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(58, 'KarinaLobanchikova', '085a6', 'KarinaLobanchikova@gmail.com', 2, 2, 'Карина', '', 'Лобанчикова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(59, 'VladaLukashevich', '9a8f6', 'VladaLukashevich@gmail.com', 2, 2, 'Влада', '', 'Лукашевич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(60, 'OlgaMartinyuk', 'dce3c', 'OlgaMartinyuk@gmail.com', 2, 2, 'Ольга', '', 'Мартинюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(61, 'OleksandrMinash', 'a0e39', 'OleksandrMinash@gmail.com', 1, 2, 'Олександр', '', 'Минаш', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(62, 'AnastasiyaPavlyuk', 'bb9bb', 'AnastasiyaPavlyuk@gmail.com', 2, 2, 'Анастасія', '', 'Павлюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(63, 'VitaliyPiontkivskiy', 'f594b', 'VitaliyPiontkivskiy@gmail.com', 1, 2, 'Віталій', '', 'Піонтківський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(64, 'OlgaRyabko', '0b751', 'OlgaRyabko@gmail.com', 2, 2, 'Ольга', '', 'Рябко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(65, 'AndriySahno', '49770', 'AndriySahno@gmail.com', 1, 2, 'Андрій', '', 'Сахно', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(66, 'NikitaSegedin', '640b3', 'NikitaSegedin@gmail.com', 1, 2, 'Нікіта', '', 'Сегедін', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(67, 'AngelinaSirota', 'aa781', 'AngelinaSirota@gmail.com', 2, 2, 'Ангеліна', '', 'Сирота', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(68, 'AndriySimchuk', '58867', 'AndriySimchuk@gmail.com', 1, 2, 'Андрій', '', 'Сімчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(69, 'IllyaFialkivskiy', '664fc', 'IllyaFialkivskiy@gmail.com', 1, 2, 'Ілля', '', 'Фіалківський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(70, 'SvitlanaCigankova', '3d942', 'SvitlanaCigankova@gmail.com', 2, 2, 'Світлана', '', 'Циганкова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(71, 'AnastasiyaShevchuk', '1a226', 'AnastasiyaShevchuk@gmail.com', 2, 2, 'Анастасія', '', 'Шевчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 2),
(72, 'MariyamAmirova', '8a132', 'MariyamAmirova@gmail.com', 2, 2, 'Маріям', '', 'Амірова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(73, 'NadiyaBeznosenko', '974b0', 'NadiyaBeznosenko@gmail.com', 2, 2, 'Надія', '', 'Безносенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(74, 'VolodimirBiblo', '37771', 'VolodimirBiblo@gmail.com', 1, 2, 'Володимир', '', 'Бібло', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(75, 'AnnaVladimirceva', '5106e', 'AnnaVladimirceva@gmail.com', 2, 2, 'Анна', '', 'Владімірцева', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(76, 'KaterinaVihristyuk', 'b9511', 'KaterinaVihristyuk@gmail.com', 2, 2, 'Катерина', '', 'Вихристюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(77, 'MariyaGalchin', '0ee6d', 'MariyaGalchin@gmail.com', 2, 2, 'Марія', '', 'Гальчин', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(78, 'VeronikaGedulyanova', '853d0', 'VeronikaGedulyanova@gmail.com', 2, 2, 'Вероніка', '', 'Гедулянова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(79, 'OlegGodlevskiy', '245a4', 'OlegGodlevskiy@gmail.com', 1, 2, 'Олег', '', 'Годлевський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(80, 'KaterinaGorecka', '954f3', 'KaterinaGorecka@gmail.com', 2, 2, 'Катерина', '', 'Горецька', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(81, 'DarinaGurelya', '88275', 'DarinaGurelya@gmail.com', 2, 2, 'Дарина', '', 'Гуреля', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(82, 'NataliyaDenisyuk', '0f087', 'NataliyaDenisyuk@gmail.com', 2, 2, 'Наталія', '', 'Денисюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(83, 'AnastasiyaKisil', '49a40', 'AnastasiyaKisil@gmail.com', 2, 2, 'Анастасія', '', 'Кисіль', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(84, 'TetyanaKorzun', '06dc1', 'TetyanaKorzun@gmail.com', 2, 2, 'Тетяна', '', 'Корзун', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(85, 'YelizavetaKrivenko', '0f97b', 'YelizavetaKrivenko@gmail.com', 2, 2, 'Єлизавета', '', 'Кривенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(86, 'MikolaKucmus', '497aa', 'MikolaKucmus@gmail.com', 1, 2, 'Микола', '', 'Куцмус', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(87, 'ReginaLisovska', '2ee65', 'ReginaLisovska@gmail.com', 2, 2, 'Регіна', '', 'Лісовська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(88, 'MariyaLopatyuk', 'ea399', 'MariyaLopatyuk@gmail.com', 2, 2, 'Марія', '', 'Лопатюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(89, 'MariyaMazko', '491b0', 'MariyaMazko@gmail.com', 2, 2, 'Марія', '', 'Мазко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(90, 'KristinaMaksimchuk', '8cf96', 'KristinaMaksimchuk@gmail.com', 2, 2, 'Крістіна', '', 'Максимчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(91, 'Dar''yaMalyuk', '776bc', 'Dar''yaMalyuk@gmail.com', 2, 2, 'Дар''я', '', 'Малюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(92, 'AlinaPavlyuk', '12f03', 'AlinaPavlyuk@gmail.com', 2, 2, 'Аліна', '', 'Павлюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(93, 'GlibPetrov', 'ce936', 'GlibPetrov@gmail.com', 1, 2, 'Гліб', '', 'Петров', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(94, 'KarinaPolischuk', '49096', 'KarinaPolischuk@gmail.com', 2, 2, 'Карина', '', 'Поліщук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(95, 'VladislavSavchenko', '8e0e7', 'VladislavSavchenko@gmail.com', 1, 2, 'Владислав', '', 'Савченко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(96, 'AnnaSemenyachenko', 'e29d4', 'AnnaSemenyachenko@gmail.com', 2, 2, 'Анна', '', 'Семеняченко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(97, 'AnastasiyaSirosh', '20b6b', 'AnastasiyaSirosh@gmail.com', 2, 2, 'Анастасія', '', 'Сірош', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(98, 'VladislavaFurihata', '5d4f5', 'VladislavaFurihata@gmail.com', 2, 2, 'Владислава', '', 'Фуріхата', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(99, 'VadimHarlamov', '2fda6', 'VadimHarlamov@gmail.com', 1, 2, 'Вадим', '', 'Харламов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(100, 'AnastasiyaCheverda', '5aa48', 'AnastasiyaCheverda@gmail.com', 2, 2, 'Анастасія', '', 'Чеверда', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(101, 'MariyaChubarova', 'e957f', 'MariyaChubarova@gmail.com', 2, 2, 'Марія', '', 'Чубарова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(102, 'MariyaShavurska', '93371', 'MariyaShavurska@gmail.com', 2, 2, 'Марія', '', 'Шавурська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(103, 'YevgeniyShahray', 'efaa4', 'YevgeniyShahray@gmail.com', 1, 2, 'Євгеній', '', 'Шахрай', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 3),
(104, 'AnastasiyaAndrushko', '77163', 'AnastasiyaAndrushko@gmail.com', 2, 2, 'Анастасія', '', 'Андрушко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(105, 'MaksimAnuchin', '5e832', 'MaksimAnuchin@gmail.com', 1, 2, 'Максим', '', 'Анучін', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(106, 'SofiyaBuhanevich', '65ff7', 'SofiyaBuhanevich@gmail.com', 2, 2, 'Софія', '', 'Буханевич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(107, 'ValeriyaVaskovska', 'b3cb1', 'ValeriyaVaskovska@gmail.com', 2, 2, 'Валерія', '', 'Васьковська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(108, 'YevgenVengerskiy', '400d2', 'YevgenVengerskiy@gmail.com', 1, 2, 'Євген', '', 'Венгерський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(109, 'KarinaVoytovich', 'e6172', 'KarinaVoytovich@gmail.com', 2, 2, 'Карина', '', 'Войтович', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(110, 'DanilDombrovskiy', '6b1b5', 'DanilDombrovskiy@gmail.com', 1, 2, 'Данил', '', 'Домбровський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(111, 'VolodimirZaharchuk', '199af', 'VolodimirZaharchuk@gmail.com', 1, 2, 'Володимир', '', 'Захарчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(112, 'IllyaIvannikov', 'cc80b', 'IllyaIvannikov@gmail.com', 1, 2, 'Ілля', '', 'Іванніков', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(113, 'MariyaIschenko', 'f9592', 'MariyaIschenko@gmail.com', 2, 2, 'Марія', '', 'Іщенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(114, 'DmitroKaminniy', '208f5', 'DmitroKaminniy@gmail.com', 1, 2, 'Дмитро', '', 'Камінний', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(115, 'DaniyilMakarov', 'cbd65', 'DaniyilMakarov@gmail.com', 1, 2, 'Даниїл', '', 'Макаров', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(116, 'RostislavMatyash', '3c5b5', 'RostislavMatyash@gmail.com', 1, 2, 'Ростислав', '', 'Матяш', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(117, 'AnatoliyMitrofanov', '82bdd', 'AnatoliyMitrofanov@gmail.com', 1, 2, 'Анатолій', '', 'Митрофанов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(118, 'MariyaMilevska', 'f51e8', 'MariyaMilevska@gmail.com', 2, 2, 'Марія', '', 'Мілевська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(119, 'VladislavMuravec', '982e0', 'VladislavMuravec@gmail.com', 1, 2, 'Владислав', '', 'Муравець', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(120, 'SergiyOmecinskiy', 'dcd89', 'SergiyOmecinskiy@gmail.com', 1, 2, 'Сергій', '', 'Омецинський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(121, 'OleksandrOsipov', '45c29', 'OleksandrOsipov@gmail.com', 1, 2, 'Олександр', '', 'Осіпов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(122, 'MikolaPavlichenko', '1cd29', 'MikolaPavlichenko@gmail.com', 1, 2, 'Микола', '', 'Павліченко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(123, 'IllyaPovoroznyuk', 'fb07c', 'IllyaPovoroznyuk@gmail.com', 1, 2, 'Ілля', '', 'Поворознюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(124, 'OleksandraProkopchuk', 'fe6aa', 'OleksandraProkopchuk@gmail.com', 2, 2, 'Олександра', '', 'Прокопчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(125, 'SofiyaSlovinska', '7430a', 'SofiyaSlovinska@gmail.com', 2, 2, 'Софія', '', 'Словінська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(126, 'NazariyTkachenko', 'aa048', 'NazariyTkachenko@gmail.com', 1, 2, 'Назарій', '', 'Ткаченко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(127, 'OleksiyFedorov', '9017b', 'OleksiyFedorov@gmail.com', 1, 2, 'Олексій', '', 'Федоров', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(128, 'TimurFedorov', '485a4', 'TimurFedorov@gmail.com', 1, 2, 'Тимур', '', 'Федоров', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(129, 'VeronikaChemeris', '9071b', 'VeronikaChemeris@gmail.com', 2, 2, 'Вероніка', '', 'Чемерис', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(130, 'MaksimChirkov', '7d873', 'MaksimChirkov@gmail.com', 1, 2, 'Максим', '', 'Чирков', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(131, 'KlavdiyaShatilovich', '280f7', 'KlavdiyaShatilovich@gmail.com', 2, 2, 'Клавдія', '', 'Шатилович', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(132, 'RomanShumlyakivskiy', '55f6d', 'RomanShumlyakivskiy@gmail.com', 1, 2, 'Роман', '', 'Шумляківський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 4),
(133, 'ViktoriyaBondar', '9b60d', 'ViktoriyaBondar@gmail.com', 2, 2, 'Вікторія', '', 'Бондар', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(134, 'TimofiyBondarchuk', '55d0a', 'TimofiyBondarchuk@gmail.com', 1, 2, 'Тимофій', '', 'Бондарчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(135, 'YuliyaBudnik', '4b41b', 'YuliyaBudnik@gmail.com', 2, 2, 'Юлія', '', 'Буднік', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(136, 'DaniloValko', 'e0203', 'DaniloValko@gmail.com', 1, 2, 'Данило', '', 'Валько', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(137, 'OleksandrVerbovskiy', 'a3f19', 'OleksandrVerbovskiy@gmail.com', 1, 2, 'Олександр', '', 'Вербовський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(138, 'ViktoriyaVlasenko', '0ae38', 'ViktoriyaVlasenko@gmail.com', 2, 2, 'Вікторія', '', 'Власенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(139, 'NikitaGrischak', '0f24e', 'NikitaGrischak@gmail.com', 1, 2, 'Нікіта', '', 'Грищак', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(140, 'VolodimirGrischenko', '57256', 'VolodimirGrischenko@gmail.com', 1, 2, 'Володимир', '', 'Грищенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(141, 'OleksandrDorosh', 'b8290', 'OleksandrDorosh@gmail.com', 1, 2, 'Олександр', '', 'Дорош', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(142, 'AndriyYeremeychuk', 'c8eec', 'AndriyYeremeychuk@gmail.com', 1, 2, 'Андрій', '', 'Єремейчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(143, 'DaniloKlusenko', '6d9ec', 'DaniloKlusenko@gmail.com', 1, 2, 'Данило', '', 'Клусенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(144, 'OleksandrKovalchuk', 'd94e3', 'OleksandrKovalchuk@gmail.com', 1, 2, 'Олександр', '', 'Ковальчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(145, 'DmitroLavrenyuk', '27d97', 'DmitroLavrenyuk@gmail.com', 1, 2, 'Дмитро', '', 'Лавренюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(146, 'V’yacheslavLobay', 'd2a7b', 'V’yacheslavLobay@gmail.com', 1, 2, 'В’ячеслав', '', 'Лобай', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(147, 'OleksandraMalecka', '47ebd', 'OleksandraMalecka@gmail.com', 2, 2, 'Олександра', '', 'Малецька', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(148, 'DaniloManzhos', '57542', 'DaniloManzhos@gmail.com', 1, 2, 'Данило', '', 'Манжос', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(149, 'YuliyaMartinyuk', 'b8c66', 'YuliyaMartinyuk@gmail.com', 2, 2, 'Юлія', '', 'Мартинюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(150, 'YevgeniyMatviychuk', '2ab50', 'YevgeniyMatviychuk@gmail.com', 1, 2, 'Євгеній', '', 'Матвійчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(151, 'DaniloMelnik', '7b37a', 'DaniloMelnik@gmail.com', 1, 2, 'Данило', '', 'Мельник', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(152, 'MihayloMoskalenko', '01fde', 'MihayloMoskalenko@gmail.com', 1, 2, 'Михайло', '', 'Москаленко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(153, 'AnnaNelipovich', '17943', 'AnnaNelipovich@gmail.com', 2, 2, 'Анна', '', 'Нелипович', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(154, 'PavloPavlichenko', '27157', 'PavloPavlichenko@gmail.com', 1, 2, 'Павло', '', 'Павліченко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(155, 'ArtemPazich', '317c3', 'ArtemPazich@gmail.com', 1, 2, 'Артем', '', 'Пазич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(156, 'BogdanPinchuk', '3e01f', 'BogdanPinchuk@gmail.com', 1, 2, 'Богдан', '', 'Пінчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(157, 'DenisSorokin', 'efe81', 'DenisSorokin@gmail.com', 1, 2, 'Денис', '', 'Сорокін', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(158, 'VladislavSatkevich', 'ed9de', 'VladislavSatkevich@gmail.com', 1, 2, 'Владислав', '', 'Саткевич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(159, 'DmitroStecko', 'cc6d5', 'DmitroStecko@gmail.com', 1, 2, 'Дмитро', '', 'Стецько', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(160, 'LiliyaChernish', '36799', 'LiliyaChernish@gmail.com', 2, 2, 'Лілія', '', 'Черниш', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(161, 'VolodimirSharkovskiy', '4bd9d', 'VolodimirSharkovskiy@gmail.com', 1, 2, 'Володимир', '', 'Шарковський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(162, 'SofiyaGukaylo', '190e9', 'SofiyaGukaylo@gmail.com', 2, 2, 'Софія', '', 'Гукайло', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(163, 'PavloFedorenko', 'ec6bf', 'PavloFedorenko@gmail.com', 1, 2, 'Павло', '', 'Федоренко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 5),
(164, 'BorislavBrodskiy', 'dae3a', 'BorislavBrodskiy@gmail.com', 1, 2, 'Борислав', '', 'Бродський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(165, 'ViktoriyaValinkevich', '7b998', 'ViktoriyaValinkevich@gmail.com', 2, 2, 'Вікторія', '', 'Валінкевич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(166, 'DianaVityuk', '415d4', 'DianaVityuk@gmail.com', 2, 2, 'Діана', '', 'Вітюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(167, 'OleksandrVolkov', 'a1d0d', 'OleksandrVolkov@gmail.com', 1, 2, 'Олександр', '', 'Волков', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(168, 'Dar''yaVolohatyuk', '87bce', 'Dar''yaVolohatyuk@gmail.com', 2, 2, 'Дар''я', '', 'Волохатюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(169, 'PolinaGolubivska', '6a12a', 'PolinaGolubivska@gmail.com', 2, 2, 'Поліна', '', 'Голубівська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(170, 'GannaGurtovenko', '30f73', 'GannaGurtovenko@gmail.com', 2, 2, 'Ганна', '', 'Гуртовенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(171, 'DavidDerevlyaniy', '3aa43', 'DavidDerevlyaniy@gmail.com', 1, 2, 'Давид', '', 'Деревляний', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(172, 'MariyaDihtiyevska', '50752', 'MariyaDihtiyevska@gmail.com', 2, 2, 'Марія', '', 'Діхтієвська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(173, 'VolodimirYelisyeyev', 'cc2b2', 'VolodimirYelisyeyev@gmail.com', 1, 2, 'Володимир', '', 'Єлісєєв', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(174, 'DaniyilYolkin', 'ef12d', 'DaniyilYolkin@gmail.com', 1, 2, 'Даніїл', '', 'Йолкін', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(175, 'AnnaKolos', '7b294', 'AnnaKolos@gmail.com', 2, 2, 'Анна', '', 'Колос', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(176, 'SofiyaKostyuchenko', '5817d', 'SofiyaKostyuchenko@gmail.com', 2, 2, 'Софія', '', 'Костюченко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(177, 'DavidKuliyev', '6c75e', 'DavidKuliyev@gmail.com', 1, 2, 'Давид', '', 'Кулієв', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(178, 'IgorKulik', '70bda', 'IgorKulik@gmail.com', 1, 2, 'Ігор', '', 'Кулік', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(179, 'AnastasiyaKutishenko', '206cd', 'AnastasiyaKutishenko@gmail.com', 2, 2, 'Анастасія', '', 'Кутишенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(180, 'RomanLazarenko', '6b92f', 'RomanLazarenko@gmail.com', 1, 2, 'Роман', '', 'Лазаренко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(181, 'DarinaLuk`yanec', '2f68f', 'DarinaLuk`yanec@gmail.com', 2, 2, 'Дарина', '', 'Лук`янець', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(182, 'DarinaMoshkivska', 'f6ad7', 'DarinaMoshkivska@gmail.com', 2, 2, 'Дарина', '', 'Мошківська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(183, 'ViktoriyaPanochishena', '6f3d4', 'ViktoriyaPanochishena@gmail.com', 2, 2, 'Вікторія', '', 'Паночишена', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(184, 'YelizavetaPereguda', 'a7ec3', 'YelizavetaPereguda@gmail.com', 2, 2, 'Єлизавета', '', 'Перегуда', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(185, 'VladislavPereder', 'efd16', 'VladislavPereder@gmail.com', 1, 2, 'Владислав', '', 'Передер', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(186, 'MihayloPiontkivskiy', '54b33', 'MihayloPiontkivskiy@gmail.com', 1, 2, 'Михайло', '', 'Піонтківський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(187, 'VitaliyPrischepa', 'f76aa', 'VitaliyPrischepa@gmail.com', 1, 2, 'Віталій', '', 'Прищепа', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(188, 'GlibRudakov', '95aec', 'GlibRudakov@gmail.com', 1, 2, 'Гліб', '', 'Рудаков', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(189, 'VladislavSkakovskiy', '50f95', 'VladislavSkakovskiy@gmail.com', 1, 2, 'Владислав', '', 'Скаковський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(190, 'OlegTrohimec', '8f160', 'OlegTrohimec@gmail.com', 1, 2, 'Олег', '', 'Трохимець', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(191, 'AnastasiyaHarina', '35d6f', 'AnastasiyaHarina@gmail.com', 2, 2, 'Анастасія', '', 'Харина', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(192, 'DianaHortyuk', 'afea8', 'DianaHortyuk@gmail.com', 2, 2, 'Діана', '', 'Хортюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(193, 'OleksandraChuprun', 'eab8a', 'OleksandraChuprun@gmail.com', 2, 2, 'Олександра', '', 'Чупрун', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(194, 'OlgaShevel', 'e5a6e', 'OlgaShevel@gmail.com', 2, 2, 'Ольга', '', 'Шевель', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(195, 'MarinaShpakivska', '25b40', 'MarinaShpakivska@gmail.com', 2, 2, 'Марина', '', 'Шпаківська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(196, 'BogdanYasinchuk', '7bc1a', 'BogdanYasinchuk@gmail.com', 1, 2, 'Богдан', '', 'Ясінчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(197, 'NazarYacuk', 'a94f0', 'NazarYacuk@gmail.com', 1, 2, 'Назар', '', 'Яцук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 6),
(198, 'OleksandrAndriychuk', 'd367f', 'OleksandrAndriychuk@gmail.com', 1, 2, 'Олександр', '', 'Андрійчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(199, 'OleksandrAfonin', '64fef', 'OleksandrAfonin@gmail.com', 1, 2, 'Олександр', '', 'Афонін', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(200, 'DarinaBargilevich', 'b3dba', 'DarinaBargilevich@gmail.com', 2, 2, 'Дарина', '', 'Баргилевич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(201, 'OleksiyBondarenko', '8e0ef', 'OleksiyBondarenko@gmail.com', 1, 2, 'Олексій', '', 'Бондаренко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(202, 'YevgenVargan', 'cdd50', 'YevgenVargan@gmail.com', 1, 2, 'Євген', '', 'Варган', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(203, 'AndriyVostrikov', 'e1c55', 'AndriyVostrikov@gmail.com', 1, 2, 'Андрій', '', 'Востріков', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(204, 'V’yacheslavGavrish', '180f7', 'V’yacheslavGavrish@gmail.com', 1, 2, 'В’ячеслав', '', 'Гавриш', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(205, 'AnatoliyGrinchuk', 'c9f7c', 'AnatoliyGrinchuk@gmail.com', 1, 2, 'Анатолій', '', 'Гринчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(206, 'NestorDenisyuk', 'a09d7', 'NestorDenisyuk@gmail.com', 1, 2, 'Нестор', '', 'Денисюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(207, 'ArturZadnipryanec', '2dff6', 'ArturZadnipryanec@gmail.com', 1, 2, 'Артур', '', 'Задніпрянець', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(208, 'ArtemKolomiyec', 'c0f8b', 'ArtemKolomiyec@gmail.com', 1, 2, 'Артем', '', 'Коломієць', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(209, 'SergiyKorniychuk', 'a232d', 'SergiyKorniychuk@gmail.com', 1, 2, 'Сергій', '', 'Корнійчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(210, 'MikitaKulbanevich', '36cb0', 'MikitaKulbanevich@gmail.com', 1, 2, 'Микита', '', 'Кульбаневич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(211, 'PavloKuzminskiy', '1a05a', 'PavloKuzminskiy@gmail.com', 1, 2, 'Павло', '', 'Кузьмінський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(212, 'DmitroKunickiy', '3efa5', 'DmitroKunickiy@gmail.com', 1, 2, 'Дмитро', '', 'Куницький', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(213, 'OleksandrLipinskiy', 'ca7e2', 'OleksandrLipinskiy@gmail.com', 1, 2, 'Олександр', '', 'Ліпінський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(214, 'IgorMaksimov', '9f6d3', 'IgorMaksimov@gmail.com', 1, 2, 'Ігор', '', 'Максимов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(215, 'AnastasiyaMozhayko', '1656e', 'AnastasiyaMozhayko@gmail.com', 2, 2, 'Анастасія', '', 'Можайко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(216, 'OleksandraNaumova', '2cd74', 'OleksandraNaumova@gmail.com', 2, 2, 'Олександра', '', 'Наумова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(217, 'VolodimirOseckiy', '7a5c6', 'VolodimirOseckiy@gmail.com', 1, 2, 'Володимир', '', 'Осецький', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(218, 'AnnaPavlik', 'e4c09', 'AnnaPavlik@gmail.com', 2, 2, 'Анна', '', 'Павлік', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(219, 'AnastasiyaPavlova', '2d507', 'AnastasiyaPavlova@gmail.com', 2, 2, 'Анастасія', '', 'Павлова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(220, 'DenisPirogov', 'f0110', 'DenisPirogov@gmail.com', 1, 2, 'Денис', '', 'Пирогов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(221, 'OleksandrPobidish', '6920c', 'OleksandrPobidish@gmail.com', 1, 2, 'Олександр', '', 'Побідиш', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(222, 'KostyantinRetivih', '30184', 'KostyantinRetivih@gmail.com', 1, 2, 'Костянтин', '', 'Ретивих', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(223, 'MihayloRudiy', '4c736', 'MihayloRudiy@gmail.com', 1, 2, 'Михайло', '', 'Рудий', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(224, 'AnastasiyaSapanus', '2239a', 'AnastasiyaSapanus@gmail.com', 2, 2, 'Анастасія', '', 'Сапанусь', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(225, 'SofiyaSereda', 'd6317', 'SofiyaSereda@gmail.com', 2, 2, 'Софія', '', 'Середа', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(226, 'MariyaTkach', '1122c', 'MariyaTkach@gmail.com', 2, 2, 'Марія', '', 'Ткач', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(227, 'OleksandrSheketa', 'ae5c6', 'OleksandrSheketa@gmail.com', 1, 2, 'Олександр', '', 'Шекета', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(228, 'MariyaYagudina', '959e8', 'MariyaYagudina@gmail.com', 2, 2, 'Марія', '', 'Ягудіна', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 7),
(229, 'LadaAvramchuk', '7c27a', 'LadaAvramchuk@gmail.com', 2, 2, 'Лада', '', 'Аврамчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(230, 'YuliyaVenglovska', 'd84ed', 'YuliyaVenglovska@gmail.com', 2, 2, 'Юлія', '', 'Венгловська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(231, 'ArtemVerbilo', '718a5', 'ArtemVerbilo@gmail.com', 1, 2, 'Артем', '', 'Вербило', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(232, 'OleksandrGorobec', 'c4d5a', 'OleksandrGorobec@gmail.com', 1, 2, 'Олександр', '', 'Горобець', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(233, 'NikitaDubenkov', '63e8d', 'NikitaDubenkov@gmail.com', 1, 2, 'Нікіта', '', 'Дубенков', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(234, 'YevgeniyEysmont', '92075', 'YevgeniyEysmont@gmail.com', 1, 2, 'Євгеній', '', 'Ейсмонт', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(235, 'OlenaZhembrovska', '78755', 'OlenaZhembrovska@gmail.com', 2, 2, 'Олена', '', 'Жембровська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(236, 'SofiyaZinyuk', 'dfe28', 'SofiyaZinyuk@gmail.com', 2, 2, 'Софія', '', 'Зінюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(237, 'DmitroKirshev', '2c83f', 'DmitroKirshev@gmail.com', 1, 2, 'Дмитро', '', 'Кіршев', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(238, 'YelizavetaKlyuchnikova', '7c713', 'YelizavetaKlyuchnikova@gmail.com', 2, 2, 'Єлизавета', '', 'Ключнікова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(239, 'MaksimKovalenko', 'edd2c', 'MaksimKovalenko@gmail.com', 1, 2, 'Максим', '', 'Коваленко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(240, 'MikitaKoshelyev', '90279', 'MikitaKoshelyev@gmail.com', 1, 2, 'Микита', '', 'Кошелєв', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(241, 'OlenaKrindach', 'df9a6', 'OlenaKrindach@gmail.com', 2, 2, 'Олена', '', 'Криндач', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(242, 'YuliyaKurbanova', '07870', 'YuliyaKurbanova@gmail.com', 2, 2, 'Юлія', '', 'Курбанова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(243, 'ViktoriyaLeschishina', '7c7f5', 'ViktoriyaLeschishina@gmail.com', 2, 2, 'Вікторія', '', 'Лещишина', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(244, 'YaroslavLimar', '91833', 'YaroslavLimar@gmail.com', 1, 2, 'Ярослав', '', 'Лимар', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(245, 'AnnaLihoglyad', '414a1', 'AnnaLihoglyad@gmail.com', 2, 2, 'Анна', '', 'Лихогляд', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(246, 'VladislavMinkovec', '532c0', 'VladislavMinkovec@gmail.com', 1, 2, 'Владислав', '', 'Міньковець', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(247, 'OleksandrMulik', 'c0cd5', 'OleksandrMulik@gmail.com', 1, 2, 'Олександр', '', 'Мулик', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8);
INSERT INTO `users` (`user_id`, `login`, `password`, `email`, `gender`, `role_id`, `first_name`, `patronymic`, `last_name`, `image`, `status_text`, `position_id`, `group_id`) VALUES
(248, 'YuriyMuschinin', 'bd9f7', 'YuriyMuschinin@gmail.com', 1, 2, 'Юрій', '', 'Мущинін', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(249, 'IvanNechiporenko', 'ec182', 'IvanNechiporenko@gmail.com', 1, 2, 'Іван', '', 'Нечипоренко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(250, 'KostyantinOgiyenko', '0c219', 'KostyantinOgiyenko@gmail.com', 1, 2, 'Костянтин', '', 'Огієнко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(251, 'IllyaPisarchuk', 'ac60f', 'IllyaPisarchuk@gmail.com', 1, 2, 'Ілля', '', 'Писарчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(252, 'IvanPokatilov', '5ca70', 'IvanPokatilov@gmail.com', 1, 2, 'Іван', '', 'Покатілов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(253, 'OleksandrSvistelnik', '6e862', 'OleksandrSvistelnik@gmail.com', 1, 2, 'Олександр', '', 'Свістельник', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(254, 'OleksandraSidorova', '8600c', 'OleksandraSidorova@gmail.com', 2, 2, 'Олександра', '', 'Сидорова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(255, 'DmitroUvin', 'adbd2', 'DmitroUvin@gmail.com', 1, 2, 'Дмитро', '', 'Увін', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(256, 'DmitroHabaznya', 'b5701', 'DmitroHabaznya@gmail.com', 1, 2, 'Дмитро', '', 'Хабазня', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(257, 'DmitroShashkevich', '0a7ad', 'DmitroShashkevich@gmail.com', 1, 2, 'Дмитро', '', 'Шашкевич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(258, 'MaksimShashkevich', '4d0f1', 'MaksimShashkevich@gmail.com', 1, 2, 'Максим', '', 'Шашкевич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 8),
(259, 'IrinaArhilyuk', 'ac031', 'IrinaArhilyuk@gmail.com', 2, 2, 'Ірина', '', 'Архілюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(260, 'MihayloBorovec', '40e13', 'MihayloBorovec@gmail.com', 1, 2, 'Михайло', '', 'Боровець', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(261, 'OleksandrDarman', '9ba10', 'OleksandrDarman@gmail.com', 1, 2, 'Олександр', '', 'Дарман', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(262, 'MilanaDobrovolska', 'f8cb1', 'MilanaDobrovolska@gmail.com', 2, 2, 'Милана', '', 'Добровольська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(263, 'PolinaDobrovolska', 'cf3e1', 'PolinaDobrovolska@gmail.com', 2, 2, 'Поліна', '', 'Добровольська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(264, 'ViktoriyaZinchuk', '4b1a2', 'ViktoriyaZinchuk@gmail.com', 2, 2, 'Вікторія', '', 'Зінчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(265, 'VeronikaIschuk', '78a90', 'VeronikaIschuk@gmail.com', 2, 2, 'Вероніка', '', 'Іщук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(266, 'ViktoriyaKoval', 'a65d4', 'ViktoriyaKoval@gmail.com', 2, 2, 'Вікторія', '', 'Коваль', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(267, 'ValeriyaKozicka', 'ecc0c', 'ValeriyaKozicka@gmail.com', 2, 2, 'Валерія', '', 'Козицька', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(268, 'ViktoriyaKrapivnicka', '3311a', 'ViktoriyaKrapivnicka@gmail.com', 2, 2, 'Вікторія', '', 'Крапивницька', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(269, 'TetyanaLukashenko', '52c14', 'TetyanaLukashenko@gmail.com', 2, 2, 'Тетяна', '', 'Лукашенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(270, 'AnastasiyaMalovana', 'b4379', 'AnastasiyaMalovana@gmail.com', 2, 2, 'Анастасія', '', 'Мальована', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(271, 'OleksandraMaslyukovska', '13089', 'OleksandraMaslyukovska@gmail.com', 2, 2, 'Олександра', '', 'Маслюковська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(272, 'DianaNovikova', '77e6d', 'DianaNovikova@gmail.com', 2, 2, 'Діана', '', 'Новікова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(273, 'ArmanOvakimyan', '38a61', 'ArmanOvakimyan@gmail.com', 1, 2, 'Арман', '', 'Овакімян', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(274, 'OleksandraOmecinska', '812c9', 'OleksandraOmecinska@gmail.com', 2, 2, 'Олександра', '', 'Омецинська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(275, 'AnastasiyaOrinska', 'ceea5', 'AnastasiyaOrinska@gmail.com', 2, 2, 'Анастасія', '', 'Оринська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(276, 'DarinaPavlenko', '562b8', 'DarinaPavlenko@gmail.com', 2, 2, 'Дарина', '', 'Павленко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(277, 'YuliyaPanchenko', '7a9c6', 'YuliyaPanchenko@gmail.com', 2, 2, 'Юлія', '', 'Панченко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(278, 'AnnaPrischepa', '32c58', 'AnnaPrischepa@gmail.com', 2, 2, 'Анна', '', 'Прищепа', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(279, 'BogdanaSemenova', '63103', 'BogdanaSemenova@gmail.com', 2, 2, 'Богдана', '', 'Семенова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(280, 'ArtemSergyeyan', 'a35f6', 'ArtemSergyeyan@gmail.com', 1, 2, 'Артем', '', 'Сергєян', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(281, 'OleksandrSinkevich', '41c45', 'OleksandrSinkevich@gmail.com', 1, 2, 'Олександр', '', 'Сінкевич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(282, 'SofiyaSlonovska', '78826', 'SofiyaSlonovska@gmail.com', 2, 2, 'Софія', '', 'Слоньовська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(283, 'MariyaSorochan', '4fc9d', 'MariyaSorochan@gmail.com', 2, 2, 'Марія', '', 'Сорочан', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(284, 'UlyanaHodakivska', 'd97b8', 'UlyanaHodakivska@gmail.com', 2, 2, 'Уляна', '', 'Ходаківська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(285, 'ValeriyaCholach', '542ad', 'ValeriyaCholach@gmail.com', 2, 2, 'Валерія', '', 'Чолач', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(286, 'VladaYurevich', 'c7682', 'VladaYurevich@gmail.com', 2, 2, 'Влада', '', 'Юревич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(287, 'YevgeniyNaumeyko', '9d48d', 'YevgeniyNaumeyko@gmail.com', 1, 2, 'Євгеній', '', 'Наумейко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 9),
(288, 'AndriyAnchis', '40d0e', 'AndriyAnchis@gmail.com', 1, 2, 'Андрій', '', 'Анчис', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(289, 'DmitroGalonza', '0d8ad', 'DmitroGalonza@gmail.com', 1, 2, 'Дмитро', '', 'Галонза', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(290, 'AnastasiyaGalchin', '8f493', 'AnastasiyaGalchin@gmail.com', 2, 2, 'Анастасія', '', 'Гальчин', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(291, 'OleksiyGlyuza', 'c91d0', 'OleksiyGlyuza@gmail.com', 1, 2, 'Олексій', '', 'Глюза', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(292, 'VitaliyGromoviy', 'a0e49', 'VitaliyGromoviy@gmail.com', 1, 2, 'Віталій', '', 'Громовий', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(293, 'OleksandrGumenyuk', '403f2', 'OleksandrGumenyuk@gmail.com', 1, 2, 'Олександр', '', 'Гуменюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(294, 'AnastasiyaDzhigora', '06bca', 'AnastasiyaDzhigora@gmail.com', 2, 2, 'Анастасія', '', 'Джигора', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(295, 'VladislavYevdokimov', '9ade4', 'VladislavYevdokimov@gmail.com', 1, 2, 'Владислав', '', 'Євдокимов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(296, 'IgorZayec', '1087b', 'IgorZayec@gmail.com', 1, 2, 'Ігор', '', 'Заєць', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(297, 'NikitaZaycev', '7c803', 'NikitaZaycev@gmail.com', 1, 2, 'Нікіта', '', 'Зайцев', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(298, 'VyacheslavKlikova', '469c1', 'VyacheslavKlikova@gmail.com', 1, 2, 'Вячеслав', '', 'Кликова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(299, 'DarinaKoval', 'b77aa', 'DarinaKoval@gmail.com', 2, 2, 'Дарина', '', 'Коваль', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(300, 'KaterinaKoshlan', '8d4da', 'KaterinaKoshlan@gmail.com', 2, 2, 'Катерина', '', 'Кошлань', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(301, 'DmitroKuchmanskiy', 'a8b00', 'DmitroKuchmanskiy@gmail.com', 1, 2, 'Дмитро', '', 'Кучманський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(302, 'AnastasiyaLisnevska', 'bb9f2', 'AnastasiyaLisnevska@gmail.com', 2, 2, 'Анастасія', '', 'Лісневська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(303, 'ArseniyNemirich', '22533', 'ArseniyNemirich@gmail.com', 1, 2, 'Арсеній', '', 'Немирич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(304, 'OlegNechuhraniy', '29c28', 'OlegNechuhraniy@gmail.com', 1, 2, 'Олег', '', 'Нечухраний', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(305, 'ArtemPanchenko', '15be4', 'ArtemPanchenko@gmail.com', 1, 2, 'Артем', '', 'Панченко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(306, 'MihayloPekaryev', '6a258', 'MihayloPekaryev@gmail.com', 1, 2, 'Михайло', '', 'Пекарєв', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(307, 'DaniloPolischuk', '2ba8b', 'DaniloPolischuk@gmail.com', 1, 2, 'Данило', '', 'Поліщук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(308, 'IgorPonomarov', 'a2c86', 'IgorPonomarov@gmail.com', 1, 2, 'Ігор', '', 'Пономарьов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(309, 'OleksiySimon', '8fecc', 'OleksiySimon@gmail.com', 1, 2, 'Олексій', '', 'Симон', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(310, 'OleksandraTashkinova', '1d059', 'OleksandraTashkinova@gmail.com', 2, 2, 'Олександра', '', 'Ташкінова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(311, 'AndriyHarchenko', '19726', 'AndriyHarchenko@gmail.com', 1, 2, 'Андрій', '', 'Харченко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(312, 'AnastasiyaHarchuk', '06321', 'AnastasiyaHarchuk@gmail.com', 2, 2, 'Анастасія', '', 'Харчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(313, 'AntonHomchuk', '25ad3', 'AntonHomchuk@gmail.com', 1, 2, 'Антон', '', 'Хомчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(314, 'ArtemChernickiy', '2e4bf', 'ArtemChernickiy@gmail.com', 1, 2, 'Артем', '', 'Черницький', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(315, 'VladislavChuprov', '3f8c5', 'VladislavChuprov@gmail.com', 1, 2, 'Владислав', '', 'Чупров', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(316, 'OleksiySharuk', '0f637', 'OleksiySharuk@gmail.com', 1, 2, 'Олексій', '', 'Шарук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(317, 'ValeriyaYaroshuk', '3fcd3', 'ValeriyaYaroshuk@gmail.com', 2, 2, 'Валерія', '', 'Ярошук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 10),
(318, 'BogdanAndriychuk', '50b3f', 'BogdanAndriychuk@gmail.com', 1, 2, 'Богдан', '', 'Андрійчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(319, 'AlisaArtyuh', 'ffdbe', 'AlisaArtyuh@gmail.com', 2, 2, 'Аліса', '', 'Артюх', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(320, 'MiroslavBartkovyak', 'c8b47', 'MiroslavBartkovyak@gmail.com', 1, 2, 'Мирослав', '', 'Бартковяк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(321, 'MariyaBasenko', 'f3e4b', 'MariyaBasenko@gmail.com', 2, 2, 'Марія', '', 'Басенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(322, 'TarasBiront', 'c8d2a', 'TarasBiront@gmail.com', 1, 2, 'Тарас', '', 'Біронт', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(323, 'DmitroVargan', 'dac6f', 'DmitroVargan@gmail.com', 1, 2, 'Дмитро', '', 'Варган', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(324, 'DaryaVolkova', 'c2b7a', 'DaryaVolkova@gmail.com', 2, 2, 'Дарья', '', 'Волкова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(325, 'ViktoriyaVorobyova', '591ac', 'ViktoriyaVorobyova@gmail.com', 2, 2, 'Вікторія', '', 'Воробйова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(326, 'IrinaGarbarchuk', 'dd890', 'IrinaGarbarchuk@gmail.com', 2, 2, 'Ірина', '', 'Гарбарчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(327, 'OleksandrGordiychuk', 'a0ea1', 'OleksandrGordiychuk@gmail.com', 1, 2, 'Олександр', '', 'Гордійчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(328, 'LinaZakrevska', 'cda24', 'LinaZakrevska@gmail.com', 2, 2, 'Ліна', '', 'Закревська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(329, 'OlgaIvanenko', '057c8', 'OlgaIvanenko@gmail.com', 2, 2, 'Ольга', '', 'Іваненко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(330, 'RostislavKiselov', '8cf4e', 'RostislavKiselov@gmail.com', 1, 2, 'Ростислав', '', 'Кисельов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(331, 'DaniyilKlimenko', '8f711', 'DaniyilKlimenko@gmail.com', 1, 2, 'Даніїл', '', 'Клименко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(332, 'MarinaKudrevich', 'f970e', 'MarinaKudrevich@gmail.com', 2, 2, 'Марина', '', 'Кудревич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(333, 'AndriyMihalchuk', '1a681', 'AndriyMihalchuk@gmail.com', 1, 2, 'Андрій', '', 'Михальчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(334, 'AntonMuzichenko', '0dfb8', 'AntonMuzichenko@gmail.com', 1, 2, 'Антон', '', 'Музиченко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(335, 'MikitaNigmatshayev', 'd5954', 'MikitaNigmatshayev@gmail.com', 1, 2, 'Микита', '', 'Нігматшаєв', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(336, 'AndriyOrischuk', '9f29a', 'AndriyOrischuk@gmail.com', 1, 2, 'Андрій', '', 'Орищук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(337, 'PavloPatinskiy', '0cd76', 'PavloPatinskiy@gmail.com', 1, 2, 'Павло', '', 'Патинський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(338, 'OleksandraRomanyuk', '0267e', 'OleksandraRomanyuk@gmail.com', 2, 2, 'Олександра', '', 'Романюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(339, 'OleksandrSamchik', '43c6e', 'OleksandrSamchik@gmail.com', 1, 2, 'Олександр', '', 'Самчик', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(340, 'VladislavSahnevich', '9c004', 'VladislavSahnevich@gmail.com', 1, 2, 'Владислав', '', 'Сахневич', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(341, 'ViktoriyaSemenyuk', 'a8781', 'ViktoriyaSemenyuk@gmail.com', 2, 2, 'Вікторія', '', 'Семенюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(342, 'ArtemSkibinskiy', 'c6659', 'ArtemSkibinskiy@gmail.com', 1, 2, 'Артем', '', 'Скібінський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(343, 'DmitroTabachenko', '0a45a', 'DmitroTabachenko@gmail.com', 1, 2, 'Дмитро', '', 'Табаченко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(344, 'ArtemCivinskiy', 'e9256', 'ArtemCivinskiy@gmail.com', 1, 2, 'Артем', '', 'Цивінський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(345, 'DmitroSheremet', 'a4801', 'DmitroSheremet@gmail.com', 1, 2, 'Дмитро', '', 'Шеремет', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(346, 'IvannaYakovenko', 'd0994', 'IvannaYakovenko@gmail.com', 2, 2, 'Іванна', '', 'Яковенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 11),
(347, 'OleksandraAmelichkina', 'f8c98', 'OleksandraAmelichkina@gmail.com', 2, 2, 'Олександра', '', 'Амелічкіна', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(348, 'KaterinaBozhenko', '4d13a', 'KaterinaBozhenko@gmail.com', 2, 2, 'Катерина', '', 'Боженко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(349, 'ValeriyaGalamay', '425dc', 'ValeriyaGalamay@gmail.com', 2, 2, 'Валерія', '', 'Галамай', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(350, 'DarinaGopanchuk', '1ff7d', 'DarinaGopanchuk@gmail.com', 2, 2, 'Дарина', '', 'Гопанчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(351, 'AnastasiyaGuz', 'c343a', 'AnastasiyaGuz@gmail.com', 2, 2, 'Анастасія', '', 'Гуз', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(352, 'YuliyaZhigunova', '0de4e', 'YuliyaZhigunova@gmail.com', 2, 2, 'Юлія', '', 'Жигунова', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(353, 'KaterinaZubik', 'b136b', 'KaterinaZubik@gmail.com', 2, 2, 'Катерина', '', 'Зубик', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(354, 'DianaKarplyuk', '63800', 'DianaKarplyuk@gmail.com', 2, 2, 'Діана', '', 'Карплюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(355, 'ValeriyaKuzovlyeva', '0ce1f', 'ValeriyaKuzovlyeva@gmail.com', 2, 2, 'Валерія', '', 'Кузовлєва', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(356, 'InnaKulikovska', '08b9d', 'InnaKulikovska@gmail.com', 2, 2, 'Інна', '', 'Куліковська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(357, 'KaterinaLisyuk', '9c126', 'KaterinaLisyuk@gmail.com', 2, 2, 'Катерина', '', 'Лисюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(358, 'GannaLitvin', '737cd', 'GannaLitvin@gmail.com', 2, 2, 'Ганна', '', 'Литвин', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(359, 'BogdanaMarischuk', 'd5be7', 'BogdanaMarischuk@gmail.com', 2, 2, 'Богдана', '', 'Марищук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(360, 'OlgaMoschicka', '410c4', 'OlgaMoschicka@gmail.com', 2, 2, 'Ольга', '', 'Мощицька', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(361, 'MikolaMuzhickiy', '9323a', 'MikolaMuzhickiy@gmail.com', 1, 2, 'Микола', '', 'Мужицький', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(362, 'AnnaMurga', 'ddcdc', 'AnnaMurga@gmail.com', 2, 2, 'Анна', '', 'Мурга', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(363, 'KristinaNovik', 'bf654', 'KristinaNovik@gmail.com', 2, 2, 'Крістіна', '', 'Новік', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(364, 'IrinaNovoselska', '3defb', 'IrinaNovoselska@gmail.com', 2, 2, 'Ірина', '', 'Новосельська', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(365, 'MaksimPirozhok', '81a0d', 'MaksimPirozhok@gmail.com', 1, 2, 'Максим', '', 'Пирожок', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(366, 'AndriyPolyakov', '4970b', 'AndriyPolyakov@gmail.com', 1, 2, 'Андрій', '', 'Поляков', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(367, 'IgorSavchenko', '6697e', 'IgorSavchenko@gmail.com', 1, 2, 'Ігор', '', 'Савченко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(368, 'YevgeniySavchuk', '649ec', 'YevgeniySavchuk@gmail.com', 1, 2, 'Євгеній', '', 'Савчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(369, 'DarinaStasyuk', '2b7fc', 'DarinaStasyuk@gmail.com', 2, 2, 'Дарина', '', 'Стасюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(370, 'VladislavChaykovskiy', '4a109', 'VladislavChaykovskiy@gmail.com', 1, 2, 'Владислав', '', 'Чайковський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(371, 'BogdanShatalov', 'e2a5d', 'BogdanShatalov@gmail.com', 1, 2, 'Богдан', '', 'Шаталов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(372, 'VladislavShahray', 'dece6', 'VladislavShahray@gmail.com', 1, 2, 'Владислав', '', 'Шахрай', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(373, 'TetyanaShvec', '4b277', 'TetyanaShvec@gmail.com', 2, 2, 'Тетяна', '', 'Швець', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(374, 'NazarYakovec', 'da344', 'NazarYakovec@gmail.com', 1, 2, 'Назар', '', 'Яковець', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 12),
(375, 'MaksimVorobey', '5243d', 'MaksimVorobey@gmail.com', 1, 2, 'Максим', '', 'Воробей', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(376, 'OleksiyGlazkov', 'eab0b', 'OleksiyGlazkov@gmail.com', 1, 2, 'Олексій', '', 'Глазков', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(377, 'RomanGolovatyuk', 'f30e0', 'RomanGolovatyuk@gmail.com', 1, 2, 'Роман', '', 'Головатюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(378, 'YuriyDemchenko', '937d6', 'YuriyDemchenko@gmail.com', 1, 2, 'Юрій', '', 'Демченко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(379, 'ArtemiyZabrockiy', '02b60', 'ArtemiyZabrockiy@gmail.com', 1, 2, 'Артемій', '', 'Заброцький', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(380, 'YevgeniyZaycev', '3d0b8', 'YevgeniyZaycev@gmail.com', 1, 2, 'Євгеній', '', 'Зайцев', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(381, 'AnnaKovbasyuk', '21d6b', 'AnnaKovbasyuk@gmail.com', 2, 2, 'Анна', '', 'Ковбасюк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(382, 'VadimKonstantinov', '25735', 'VadimKonstantinov@gmail.com', 1, 2, 'Вадим', '', 'Константінов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(383, 'BogdanKorev', '8b55d', 'BogdanKorev@gmail.com', 1, 2, 'Богдан', '', 'Корев', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(384, 'MikolaKorobchinskiy', 'c1874', 'MikolaKorobchinskiy@gmail.com', 1, 2, 'Микола', '', 'Коробчинський', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(385, 'PavloKrivenko', '6aa5c', 'PavloKrivenko@gmail.com', 1, 2, 'Павло', '', 'Кривенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(386, 'OlegLegenchuk', 'cb47d', 'OlegLegenchuk@gmail.com', 1, 2, 'Олег', '', 'Легенчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(387, 'RostislavLeh', 'face1', 'RostislavLeh@gmail.com', 1, 2, 'Ростислав', '', 'Лех', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(388, 'NestorLitvinchuk', 'f9f92', 'NestorLitvinchuk@gmail.com', 1, 2, 'Нестор', '', 'Литвинчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(389, 'ArtemLitvinov', '4fe4e', 'ArtemLitvinov@gmail.com', 1, 2, 'Артем', '', 'Літвінов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(390, 'MaksimLunyak', 'fbc86', 'MaksimLunyak@gmail.com', 1, 2, 'Максим', '', 'Луняк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(391, 'MaksimMisliviy', 'fcc18', 'MaksimMisliviy@gmail.com', 1, 2, 'Максим', '', 'Мисливий', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(392, 'YaroslavNaumov', '35a3a', 'YaroslavNaumov@gmail.com', 1, 2, 'Ярослав', '', 'Наумов', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(393, 'ArtemPalamarchuk', '47394', 'ArtemPalamarchuk@gmail.com', 1, 2, 'Артем', '', 'Паламарчук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(394, 'MaksimSitaylo', '9acd1', 'MaksimSitaylo@gmail.com', 1, 2, 'Максим', '', 'Сітайло', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(395, 'YanaTarasenko', '52e04', 'YanaTarasenko@gmail.com', 2, 2, 'Яна', '', 'Тарасенко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(396, 'IllyaTkachuk', '12331', 'IllyaTkachuk@gmail.com', 1, 2, 'Ілля', '', 'Ткачук', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(397, 'BogdanTokar', 'c015e', 'BogdanTokar@gmail.com', 1, 2, 'Богдан', '', 'Токар', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(398, 'DenisHomenko', '93886', 'DenisHomenko@gmail.com', 1, 2, 'Денис', '', 'Хоменко', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(399, 'ViktorChirchik', 'f9c61', 'ViktorChirchik@gmail.com', 1, 2, 'Віктор', '', 'Чирчик', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(400, 'MikitaShaburov', '43b74', 'MikitaShaburov@gmail.com', 1, 2, 'Микита', '', 'Шабуров', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(401, 'MariyaShulyak', 'a4c97', 'MariyaShulyak@gmail.com', 2, 2, 'Марія', '', 'Шуляк', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13),
(402, 'MihayloYaropovec', '32d3a', 'MihayloYaropovec@gmail.com', 1, 2, 'Михайло', '', 'Яроповець', '', 'Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.', 1, 13);

-- --------------------------------------------------------

--
-- Структура таблицы `user_to_position`
--

CREATE TABLE IF NOT EXISTS `user_to_position` (
  `user_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `communities`
--
ALTER TABLE `communities`
  ADD PRIMARY KEY (`community_id`);

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `community_id` (`community_id`);

--
-- Индексы таблицы `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`position_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Индексы таблицы `user_to_position`
--
ALTER TABLE `user_to_position`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `position_id` (`position_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `communities`
--
ALTER TABLE `communities`
  MODIFY `community_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `positions`
--
ALTER TABLE `positions`
  MODIFY `position_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=403;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
