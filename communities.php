<?php
    include("bootstrap.php");
    
    $breadcrumbs = [
        "index.php" => "Головна",
        "communities.php" => "Спільноти ліцею",
    ];
    
    $communities = Community::getAllCommunities();
    include('views/communities.php');
    


