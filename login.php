<?php

include("bootstrap.php");

if (isset($_POST['action']) && $_POST['action'] == 'login') {
    $userArray = User::checkLogin();
    if (!empty($userArray)) {
        $_SESSION["global_user"] = $userArray;
        header("Location: /index.php?action=login_success");
    } else {
        $messages[] = ["alert-danger", "Невірно введений логін чи пароль"];
    }
} elseif (isset($_GET["action"]) && $_GET["action"] == "logout") {
    session_destroy();
    header("Location: /index.php?action=logout_success");
}

include 'views/login.php';


