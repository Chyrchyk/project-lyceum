<?php

include("bootstrap.php");

if ($globalUser->roleId == 1) {
    header("Location: login.php");
} 

if ($globalUser->roleId < 4 && $globalUser->userId != $_REQUEST["user_id"]) {
    header("Location: index.php");
}

$userId = $_REQUEST['user_id'];
$user = new User($userId);

if (isset($_POST['action']) && $_POST['action'] == 'user_edit') {
    if ($user->update()) {
        $messages[] = ["alert-success", "Ви успішно відредагували профіль"];
    } else {
        $messages[] = ["alert-danger", "Помилка при редагуванні профілю"];
    }
}


$group = new Group($user->groupId);
$community = new Community($group->communityId);

$breadcrumbs = [
    "index.php" => "Головна",
    "communities.php" => "Спільноти ліцею",
    "community.php?community_id=" . $community->communityId => $community->name,
    "group.php?group_id=" . $group->groupId => $group->name,
    "user.php?user_id=" . $user->userId => $user->firstName . " " . $user->lastName,
];

include 'views/user_edit.php';
