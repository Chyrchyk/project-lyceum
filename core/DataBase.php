<?php

    class DataBase
    {
        public static $connection;
        
        public static function connect()
        {
            $host = "localhost";
            $port = 8888;
            $dbName = "demo_lyceum";
            $userName = "root";
            $password = "";

            //data source name
            $dsn = "mysql:host=$host;dbname=$dbName;port=$port;charset=utf8";

            //Створюємо обєкт для підєднання до бази
            self::$connection = new PDO($dsn, $userName, $password);
            //Встановлюємо атрибут для отримання масиву із запиту як асоціативного масиву
            self::$connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); 
            self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
    }
