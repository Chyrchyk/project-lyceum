<?php

spl_autoload_register(function($className) {
    $dirs = ["classes", "core"];
    foreach($dirs as $dir) {
        $file = ROOT . "/" . $dir . "/" . $className . ".php";
        if (is_file($file)) {
            include($file);
            break;
        }
    }
});
