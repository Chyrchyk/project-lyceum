<?php

class Model 
{
    public function initObjectFromArray($array)
    {
        foreach($array as $key => $value)
        {
            $property = lcfirst(str_replace("_", "", ucwords($key, "_")));
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }
}
