<?php

    define("ROOT", __DIR__);
    include(ROOT . "/core/autoload.php");
    DataBase::connect();
    session_start();
    $globalUser = new User();
    if (isset($_SESSION['global_user'])) {
        $globalUser->initObjectFromArray($_SESSION['global_user']);
    }
    $messages = [];