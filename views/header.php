<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <link rel="stylesheet" href="/views/style.css">
    </head>
    <body>
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
              <a class="navbar-brand" href="#">
                  <img src="/images/logo.png" width="70" height="70" alt="">
              </a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                    <a class="nav-link" href="/index.php">Домашня <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/communities.php">Спільноти</a>
                  </li>
                  <?php if ($globalUser->roleId > 1): ?>
                  <li class="nav-item">
                      <a class="nav-link" href="/user.php?user_id=<?= $globalUser->userId; ?>">Моя сторінка</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/login.php?action=logout">Вийти</a>
                  </li>
                  <?php else: ?>
                  <li class="nav-item">
                    <a class="nav-link" href="/login.php">Увійти</a>
                  </li>
                  <? endif; ?>
                </ul>
                  <span class="navbar-text">
                      Ласкаво просимо, <strong><?= $globalUser->firstName ?> <?= $globalUser->lastName ?></strong>
                  </span>
              </div>
            </nav>
        </div>
        
        <?php if (!empty($messages)): ?>
        <div class="container">
            <?php foreach ($messages as $messageItem): ?>
            <div class="alert <?= $messageItem[0]; ?>"><?= $messageItem[1]; ?></div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>
        
        <?php if (!empty($breadcrumbs)): ?>
        <div class="container">
            <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as $key => $bcItem): ?>
                <li class="breadcrumb-item">
                    <a href="<?= $key; ?>"><?= $bcItem; ?></a>
                </li>
            <?php endforeach; ?>
            </ol>
        </div>
        <?php endif; ?>
        
        <div id="content"><!--Begin of content section-->