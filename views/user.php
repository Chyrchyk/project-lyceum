<?php include 'views/header.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-3">
            <div class="list-group">
                <div class="list-group-item list-group-item-dark"><?= $group->name; ?></div>
                <?php foreach ($group->users as $userItem): ?>
                <a href="/user.php?user_id=<?= $userItem['user_id']; ?>" class="list-group-item list-group-item-action <?php if($userId == $userItem['user_id']): ?>active<?php endif;?>" >
                    <?= $userItem['first_name']; ?> <?= $userItem['last_name']; ?>
                </a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-sm-12 col-md-9">
            <div class="media">
                <img class="d-flex align-self-center mr-3" src="images/users/<?= $user->mainImage; ?>" alt="Generic placeholder image">
                <div class="media-body">
                    <h4 class="mt-0"><?= $user->firstName ?> <?= $user->lastName ?></h4>
                    <h5 class="mt-0"><?= $user->title ?></h5>
                    <p><?= $user->statusText ?></p>
                    <?php if ($globalUser->userId == $user->userId || 
                            $globalUser->roleId >= 4): ?>
                    <p>
                        <a href="user_edit.php?user_id=<?= $user->userId; ?>" class="btn btn-primary">Редагувати профіль</a>
                    </p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
</div>
<?php include 'views/footer.php'; ?>