<?php include 'views/header.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-3">
            <div class="list-group">
                <div class="list-group-item list-group-item-dark">Спільноти</div>
                <?php foreach ($communities as $communityItem): ?>
                <a href="/community.php?community_id=<?= $communityItem['community_id']; ?>" class="list-group-item list-group-item-action <?php if($communityId == $communityItem['community_id']): ?>active<?php endif;?>" >
                    <?= $communityItem['name']; ?>
                </a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-9">
            <div class="card">
                <div class="card-body">
                <h1 class="card-title text-center"><?= $community->name; ?></h1>
                <p class="card-text"><?= $community->description; ?></p>
                <div class="list-group">
                    <div class="list-group-item active">Групи даної спільноти</div>
                    <?php foreach ($community->groups as $group): ?>
                    <a href="group.php?group_id=<?= $group['group_id'] ?>" class="list-group-item list-group-item-action"><?= $group['name'] ?></a>
                    <?php endforeach; ?>
                </div>
                </div>
            </div>
        </div>
    </div>
    
</div>

<?php include 'views/footer.php'; ?>