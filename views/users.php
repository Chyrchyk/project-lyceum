<?php include 'views/header.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-9">
            
            <div class="list-group">
                <?php foreach ($users as $user): ?>
                <a href="profile.php?user_id=<?= $user['user_id'] ?>" class="list-group-item list-group-item-action"><?= $user['first_name'] ?> <?= $user['last_name']; ?></a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    
</div>

<?php include 'views/footer.php'; ?>