<?php include 'views/header.php'; ?>
<div class="container">
<div class="jumbotron">
    <h1 class="display-3">Житомирський міський<br> ліцей при ЖДТУ</h1>
    <p class="lead">Соціальна мережа для учнів, випускників, батьків та вчителів ліцею</p>
    <hr class="my-4">
    <p class="lead">
        <?php if ($globalUser->roleId > 1): ?>
            <a class="btn btn-primary btn-lg" href="/login.php?action=logout" role="button">Вийти</a>
        <?php else: ?>
            <a class="btn btn-primary btn-lg" href="/login.php" role="button">Увійти</a>
        <? endif; ?>
    </p>
</div>
</div>
<?php include 'views/footer.php'; ?>