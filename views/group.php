<?php include 'views/header.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-3">
            <div class="list-group">
                <div class="list-group-item list-group-item-dark"><?= $community->name; ?></div>
                <?php foreach ($community->groups as $groupItem): ?>
                <a href="/group.php?group_id=<?= $groupItem['group_id']; ?>" class="list-group-item list-group-item-action <?php if($groupId == $groupItem['group_id']): ?>active<?php endif;?>" >
                    <?= $groupItem['name']; ?>
                </a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-9">
            <h1><?= $group->name; ?></h1>
            
            <div class="list-unstyled">
                <?php foreach ($group->users as $user): ?>
                <div class="media border">
                    <img class="d-flex mr-3 p-1" style='width: 10%;' src="images/users/<?= $user['main_image']; ?>" alt="Generic placeholder image">
                    <div class="media-body">
                        <h5 class="mt-1"><a href="user.php?user_id=<?= $user['user_id'] ?>"><?= $user['first_name'] ?> <?= $user['last_name']; ?></a></h5>
                        <?= $user['status_text'] ?>
                    </div>
                </div>
                
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    
</div>

<?php include 'views/footer.php'; ?>