<?php include 'views/header.php'; ?>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-xs-12 col-md-6">
            <h1>Редагування профілю</h1>
            <form action="user_edit.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="status_text">Опишіть себе якомога найкоротше</label>
                    <textarea class="form-control" name="status_text" id="status_text" placeholder="Ваш статус"><?= $user->statusText; ?></textarea>
                </div>
                <div class="form-group">
                    <label for="image">Ваше фото (до 2Мб)</label>
                    <input type="file" class="form-control" name="image" id="image">
                </div>
                <input type="hidden" name="action" value="user_edit">
                <input type="hidden" name="user_id" value="<?= $user->userId; ?>">
                <input type="submit" class="btn btn-primary" value="Змінити">
            </form>
        </div>
    </div>
</div>
<?php include 'views/footer.php'; ?>