<?php include 'views/header.php'; ?>
<div class="container">
    <h1 class="text-center">Спільноти ліцею</h1>
    <div class="row justify-content-md-center">
        <?php foreach($communities as $community): ?>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="card" style="">
                <div class="card-body">
                    <h4 class="card-title">
                        <a href="community.php?community_id=<?= $community['community_id']; ?>">
                            <?= $community['name']; ?>
                        </a>
                    </h4>
                    <p class="card-text"><?= $community['description']; ?></p>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
    
</div>

<?php include 'views/footer.php'; ?>