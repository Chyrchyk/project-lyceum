<?php include 'views/header.php'; ?>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-xs-12 col-md-6 col-lg-4">
            <form action="/login.php" method="post">
                <div class="form-group">
                    <label for="login">Логін</label>
                    <input type="text" class="form-control" name="login" id="login" placeholder="Ваш логін">
                </div>
                <div class="form-group">
                    <label for="password">Пароль</label>
                    <input type="text" class="form-control" name="password" id="password" placeholder="Ваш пароль">
                </div>
                <input type="hidden" name="action" value="login">
                <input type="submit" class="btn btn-primary" value="Увійти">
            </form>
        </div>
    </div>
</div>
<?php include 'views/footer.php'; ?>