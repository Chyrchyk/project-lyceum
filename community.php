<?php

    include("bootstrap.php");
    $communityId = (isset($_GET['community_id'])) ? $_GET['community_id'] : 2;    
    $community = new Community($communityId); //Вся інформація про групу, в т.ч. список її студентів
    $communities = Community::getAllCommunities();
    
    $breadcrumbs = [
        "index.php" => "Головна",
        "communities.php" => "Спільноти ліцею",
        "community.php?community_id=" . $communityId => $community->name,
    ];
    include('views/community.php');
    


