<?php

    include("bootstrap.php");
    $action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : "";

    if ($action == 'logout_success') {
        $messages[] = ["alert-success", "Ви успішно вийшли із мережі!"];
    } elseif ($action == 'login_success') {
        $messages[] = ["alert-success", "Ласкаво просимо у мережі!"];
    }

    include('views/index.php');
    


