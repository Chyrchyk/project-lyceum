<?php

include("bootstrap.php");

$userId = (isset($_GET['user_id'])) ? $_GET['user_id'] : 0;
$user = new User($userId);
$group = new Group($user->groupId);
$community = new Community($group->communityId);

$breadcrumbs = [
    "index.php" => "Головна",
    "communities.php" => "Спільноти ліцею",
    "community.php?community_id=" . $community->communityId => $community->name,
    "group.php?group_id=" . $group->groupId => $group->name,
    "user.php?user_id=" . $user->userId => $user->firstName . " " . $user->lastName,
];

include 'views/user.php';


