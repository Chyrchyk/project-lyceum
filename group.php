<?php

    include("bootstrap.php");    
    $groupId = (isset($_GET['group_id'])) ? $_GET['group_id'] : 2;
    $group = new Group($groupId);
    $community = new Community($group->communityId);
    
    $breadcrumbs = [
        "index.php" => "Головна",
        "communities.php" => "Спільноти ліцею",
        "community.php?community_id=" . $community->communityId => $community->name,
        "group.php?group_id=" . $group->groupId => $group->name,
    ];
    
    include('views/group.php');
    


