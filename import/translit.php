<?php
function rus2translit($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '',  'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
        'ї' => 'yi',   'є' => 'ye',  'і' => 'i',
        
        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        'Ї' => 'Yi',   'Є' => 'Ye',  'І' => 'I',
        
    );
    return strtr($string, $converter);
}
function str2url($str) {
    // переводим в транслит
    $str = rus2translit($str);
    // в нижний регистр
    $str = ucwords($str);
    $str = str_replace(" ", "", $str);
    $str = str_replace("\'", "", $str);
    
    return $str;
}

include("../core/DataBase.php");
DataBase::connect();
try {
$sql = "SELECT * FROM users";
$stmt = DataBase::$connection->prepare($sql);
$stmt->execute();
$users = $stmt->fetchAll();
$sql1 = "UPDATE users SET login=?, password=?, email=? WHERE (user_id=?);";
$stmt1 = DataBase::$connection->prepare($sql1);
foreach($users as $user) {
    $login = str2url($user['first_name'].$user['last_name']);
    $password = substr(md5($login) . random(0, 10000), 0, 5);
    $email = $login . "@gmail.com";
    $stmt1->execute([
        $login,
        $password,
        $email,
        (int)$user['user_id'],
    ]);
}
} catch (PDOException $e) {
    echo $e->getMessage();
}

