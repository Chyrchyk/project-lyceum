<?php

class User extends Model
{
    public $userId;
    public $login;
    public $password;
    public $email;
    public $gender;
    public $roleId;
    public $firstName;
    public $patronymic;
    public $lastName;
    public $image;
    public $mainImage;
    public $statusText;
    public $positionId;
    public $title;
    public $groupId;
    public $positions;
    
    
    public function __construct($userId = 0)
    {
        if ($userId > 0) {
            $user = $this->getUserById($userId);
            $this->initObjectFromArray($user);
            if (empty($this->image)) {
                $this->mainImage = "default_" . $this->gender . rand(1, 8) . ".jpg";
            } else {
                $this->mainImage = $this->userId . "/200_" . $this->image;
            }
        } else {
            $this->userId = 0;
            $this->firstName = "Гість";
            $this->lastName = "";
            $this->roleId = 1;
        }
    }
    
    private function getUserById($userId)
    {
        $sql = "SELECT * "
                . "FROM users AS a "
                . "LEFT JOIN positions AS b "
                . "ON a.position_id=b.position_id "
                . "WHERE (user_id=?)";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute([$userId]);
        return $stmt->fetch();
    }    
    
    public static function getUsersByGroup($groupId)
    {
        $sql = "SELECT * FROM users WHERE group_id=?";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute([$groupId]);
        $users = $stmt->fetchAll();
        foreach ($users as &$user) {
            if ($user['image'] == "") {
                $user['main_image'] = "default_" . $user['gender'] . rand(1, 8) . ".jpg";
            } else {
                $user['main_image'] = $user['user_id'] . "/100_" . $user['image'];
            }
        }
        return $users;
    }
    
    public static function getAllUsers()
    {
        $sql = "SELECT * FROM users WHERE gender=1";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute([]);
        return $stmt->fetchAll(); 
    }
    
    public static function checkLogin()
    {
        $login = $_POST['login'];
        $password = $_POST['password'];
        $sql = "SELECT * FROM users WHERE (login=? AND password=?)";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute([$login, $password]);
        return $stmt->fetch();
    }
    
    public function update() 
    {
        $oldImage = $this->image;
        $this->statusText = $_POST['status_text'];
        if ($newImage = Image::setImage('users/' . $this->userId, $oldImage)) {
            $this->image = $newImage;
        } else {
            $this->image = $oldImage;
        };
        $sql = "UPDATE users SET "
                . "status_text=?, "
                . "image=? "
                . "WHERE user_id=?";
        $stmt = DataBase::$connection->prepare($sql);
        $result = $stmt->execute([
            $this->statusText,
            $this->image,
            $this->userId,
        ]);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

}
