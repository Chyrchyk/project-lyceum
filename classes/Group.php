<?php

class Group extends Model
{

    public $groupId;
    public $name;
    public $communityId;
    public $users = [];

    public static function getAllGroups()
    {
        $sql = "SELECT * FROM groups WHERE group_id>1";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function __construct($groupId) 
    {
        $this->groupId = $groupId;
        $group = $this->getGroupById($groupId);
        if (!empty($group)) {
            $this->initObjectFromArray($group);
            $this->users = User::getUsersByGroup($groupId);
        } else {
            $this->name = "Група не існує";
        }
    }
    
    private function getGroupById($groupId)
    {
        $sql = "SELECT * FROM groups WHERE (group_id=?)";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute([$groupId]);
        $group = $stmt->fetch();
        return $group;
    }
    
    public static function getGroupsByCommunity($communityId)
    {
        $sql = "SELECT * FROM groups WHERE community_id=?";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute([$communityId]);
        return $stmt->fetchAll();
    }
}
