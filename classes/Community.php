<?php

class Community extends Model
{

    public $communityId;
    public $name;
    public $description;
    public $groups = [];

    public static function getAllCommunities()
    {
        $sql = "SELECT * FROM communities WHERE community_id>1";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function __construct($communityId) 
    {
        $this->communityId = $communityId;
        $community = $this->getCommunityById($communityId);
        if (!empty($community)) {
            $this->initObjectFromArray($community);
            $this->groups = Group::getGroupsByCommunity($communityId);
        } else {
            $this->name = "Спільнота не існує";
        }
    }
    
    private function getCommunityById($communityId)
    {
        $sql = "SELECT * FROM communities WHERE (community_id=?)";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute([$communityId]);
        return $stmt->fetch();
    }            
}
